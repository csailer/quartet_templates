from django.apps import AppConfig


class QuartetTemplatesConfig(AppConfig):
    name = 'quartet_templates'
